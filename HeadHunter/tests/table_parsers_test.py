from unittest import TestCase
from scrapy import Selector, Item, Field
from ..utils import HorizontalTableParser, VerticalTableParser

HORIZONTAL_TABLE_HTML = """
<table>
    <tr>
        <td>Key 1</td>
        <td>Key 2</td>
        <td>Key 3</td>
    </tr>
    <tr>
        <td>Line 1 Value 1</td>
        <td>Line 1 Value 2</td>
        <td>Line 1 Value 3</td>
    </tr>
    <tr>
        <td>Line 2 Value 1</td>
        <td>Line 2 Value 2</td>
        <td>Line 2 Value 3</td>
    </tr>
</table>
"""

VERTICAL_TABLE_HTML = """
<table>
    <tr>
        <td>Key 1</td>
        <td>Column 1 Value 1</td>
        <td>Column 2 Value 1</td>
    </tr>
    <tr>
        <td>Key 2</td>
        <td>Column 1 Value 2</td>
        <td>Column 2 Value 2</td>
    </tr>
    <tr>
        <td>Key 3</td>
        <td>Column 1 Value 3</td>
        <td>Column 2 Value 3</td>
    </tr>
</table>
"""

DATA_MAP = {
    'key1': 'Key 1',
    'key2': 'Key 2',
    'key3': 'Key 3',
}


class TestItem(Item):
    """Item для тестирования"""
    key1 = Field()
    key2 = Field()
    key3 = Field()


class HorizontalTableParserTest(TestCase):
    """Тест для проверки основных методов парсера горизонтальных таблиц"""

    def setUp(self):
        table = Selector(text=HORIZONTAL_TABLE_HTML)
        self.htp = HorizontalTableParser(
            table_selector=table,
            data_map=DATA_MAP
        )

    def test_get_table_from_selector(self):
        """Проверка на правильность построения таблицы"""
        table_array = [
            ['Key 1', 'Key 2', 'Key 3'],
            ['Line 1 Value 1', 'Line 1 Value 2', 'Line 1 Value 3'],
            ['Line 2 Value 1', 'Line 2 Value 2', 'Line 2 Value 3'],
        ]
        self.assertEqual(self.htp.get_table_from_selector(), table_array)

    def test_get_head(self):
        """Проверка на правильность получения шапки таблицы"""
        head = {
            'Key 1': 0,
            'Key 2': 1,
            'Key 3': 2,
        }
        self.assertEqual(self.htp.get_head(), head)

    def test_get_body(self):
        """Проверка на правильность получения тела таблицы"""
        body = [
            ['Line 1 Value 1', 'Line 1 Value 2', 'Line 1 Value 3'],
            ['Line 2 Value 1', 'Line 2 Value 2', 'Line 2 Value 3'],
        ]
        self.assertEqual(self.htp.get_body(), body)

    def test_fill_item(self):
        """Проверка на правильность заполнения item'а"""
        # Предположительные данные для заполнения item'а
        data_list = ['Value 1', 'Value 2', 'Value 3']
        # Заполняем item вручную
        test_item = TestItem()
        test_item['key1'] = data_list[0]
        test_item['key2'] = data_list[1]
        test_item['key3'] = data_list[2]
        self.assertEqual(self.htp._fill_item(data_list), test_item)

    def test_get_item_list(self):
        """Проверка на правильность получения item'ов из таблицы"""
        # Создаём два тестовых item'а
        test_item1 = TestItem(
            key1='Line 1 Value 1',
            key2='Line 1 Value 2',
            key3='Line 1 Value 3'
        )
        test_item2 = TestItem(
            key1='Line 2 Value 1',
            key2='Line 2 Value 2',
            key3='Line 2 Value 3',
        )
        # Помещаем item'ы в список
        test_item_list = [test_item1, test_item2]
        self.assertEqual(self.htp.get_item_list(), test_item_list)

    def test_get_item(self):
        """Проверка на правильность получения item'а"""
        # Создаём тестовый item
        test_item = TestItem(
            key1='Line 1 Value 1',
            key2='Line 1 Value 2',
            key3='Line 1 Value 3'
        )
        self.assertEqual(self.htp.get_item(), test_item)


class VerticalTableParserTest(TestCase):
    """Тест для проверки основных методов парсера горизонтальных таблиц"""

    def setUp(self):
        table = Selector(text=VERTICAL_TABLE_HTML)
        self.vtp = VerticalTableParser(
            table_selector=table,
            data_map=DATA_MAP
        )

    def test_get_table_from_selector(self):
        """Проверка на правильность построения таблицы"""
        table_array = [
            ['Key 1', 'Column 1 Value 1', 'Column 2 Value 1'],
            ['Key 2', 'Column 1 Value 2', 'Column 2 Value 2'],
            ['Key 3', 'Column 1 Value 3', 'Column 2 Value 3'],
        ]
        self.assertEqual(self.vtp.get_table_from_selector(), table_array)

    def test_get_head(self):
        """Проверка на правильность получения шапки таблицы"""
        head = {
            'Key 1': 0,
            'Key 2': 1,
            'Key 3': 2,
        }
        self.assertEqual(self.vtp.get_head(), head)

    def test_get_body(self):
        """Проверка на правильность получения тела таблицы"""
        body = [
            ['Column 1 Value 1', 'Column 1 Value 2', 'Column 1 Value 3'],
            ['Column 2 Value 1', 'Column 2 Value 2', 'Column 2 Value 3'],
        ]
        self.assertEqual(self.vtp.get_body(), body)

    def test_get_item_list(self):
        """Проверка на правильность получения item'ов из таблицы"""
        # Создаём два тестовых item'а
        test_item1 = TestItem(
            key1='Column 1 Value 1',
            key2='Column 1 Value 2',
            key3='Column 1 Value 3'
        )
        test_item2 = TestItem(
            key1='Column 2 Value 1',
            key2='Column 2 Value 2',
            key3='Column 2 Value 3',
        )
        # Помещаем item'ы в список
        test_item_list = [test_item1, test_item2]
        self.assertEqual(self.vtp.get_item_list(), test_item_list)

    def test_get_item(self):
        """Проверка на правильность получения item'а"""
        # Создаём тестовый item
        test_item = TestItem(
            key1='Column 1 Value 1',
            key2='Column 1 Value 2',
            key3='Column 1 Value 3'
        )
        self.assertEqual(self.vtp.get_item(), test_item)
