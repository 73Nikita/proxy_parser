from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship


engine = create_engine('mysql+pymysql://root:1234@localhost/test?charset=utf8')

Session = sessionmaker(bind=engine)
session = Session()


Base = declarative_base()


class Company(Base):
    __tablename__ = 'company'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    url = Column(String)
    vacancy = relationship('Vacancy')

    def __repr__(self):
        return "<Company ('%s')>" % self.name


class Vacancy(Base):
    __tablename__ = 'vacancy'
    id = Column(Integer, primary_key=True)
    url = Column(String)
    company = Column(ForeignKey('company.id'))
    snapshot = relationship('VacancySnapshot')

    def __repr__(self):
        return "<Vacancy ('%s')>" % self.id


class VacancySnapshot(Base):
    __tablename__ = 'vacancy_snapshot'
    id = Column(Integer, primary_key=True)
    vacancy = Column(ForeignKey('vacancy.id'))
    position = Column(String)
    salary_from = Column(Integer, nullable=True)
    salary_to = Column(Integer, nullable=True)
    city = Column(String)
    experience = Column(String)
    requirement = Column(String)  # Требования
    responsibility = Column(String)  # Обязанности
    conditions = Column(String)  # Условия
    employment = Column(String)  # Тип занятости

    def __init__(self, **kwargs):
        self.vacancy = kwargs.get('vacancy')
        self.position = kwargs.get('position')
        self.salary_from = kwargs.get('salary_from')
        self.salary_to = kwargs.get('salary_to')
        self.city = kwargs.get('city')
        self.experience = kwargs.get('experience')
        self.requirement = kwargs.get('requirement')
        self.responsibility = kwargs.get('responsibility')
        self.conditions = kwargs.get('conditions')
        self.employment = kwargs.get('employment')

    def __repr__(self):
        return "<VacancySnapshot ('%s')>" % self.id
