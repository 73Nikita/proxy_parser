from HeadHunter.db_tables import session, Company, Vacancy, VacancySnapshot


class HeadhunterPipeline(object):
    item = None

    def __init__(self):
        self.vacancy_ids = set()
        self.dropped_item_count = 0

    @staticmethod
    def get_company(item):
        url = item['company_alternate_url']
        company = session.query(Company).filter(Company.url == url).first()
        if not company:
            company = Company()
            company.name = item['company_name']
            company.url = url
            session.add(company)
        return company

    @staticmethod
    def get_vacancy(item):
        url = item['alternate_url']
        vacancy = session.query(Vacancy).filter(Vacancy.url == url).first()
        if not vacancy:
            vacancy = Vacancy()
            vacancy.url = url
            session.add(vacancy)
        return vacancy

    def process_item(self, item, spider):
        vacancy_snapshot = VacancySnapshot(**item)

        vacancy = self.get_vacancy(item)
        vacancy.snapshot.append(vacancy_snapshot)

        company = self.get_company(item)
        company.vacancy.append(vacancy)

        session.add(vacancy_snapshot)
        return item

    def close_spider(self, spider):
        session.commit()
