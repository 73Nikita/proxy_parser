from scrapy import Spider, Request
from scrapy.utils.response import open_in_browser
from urllib import parse
from ..utils import HorizontalTableParser
import json
import re


class FoxtoolsSpider(Spider):
    """Spider для получения данных с api.foxtools.ru
    Примечания:
    - Есть фильтр по типу
    """
    name = 'foxtools_proxy'
    start_url = 'http://api.foxtools.ru/v2/Proxy?cp=UTF-8&lang=Auto' \
                '&type=HTTPS&free=Yes&formatting=1&page={page_number}'

    def start_requests(self):
        url = self.start_url.format(page_number=1)
        yield Request(url, callback=self.parse)

    def parse(self, response):
        data = json.loads(response.body.decode('utf-8'))
        if 'response' in data:
            for item in data['response']['items']:
                yield item

        page_count = data['response']['pageCount']
        page_number = data['response']['pageNumber']
        if page_number < page_count:
            url = self.start_url.format(page_number=page_number+1)
            yield Request(url, callback=self.parse)


class HidemySpider(Spider):
    """Spider для парсинга сайта hidemy.name
    Примечания:
    - Жёстко банят при частых запросах страницы
    """

    name = 'hidemy_proxy'
    start_urls = ['https://hidemy.name/ru/proxy-list/?type=s#list']

    def parse(self, response):
        data_map = {
            'last_update': 'Последнее обновление',
            'speed': 'Скорость',
            'country_and_city': 'Страна, город',
            'anonymouse': 'Аноним ность',
            'ip': 'IP адрес',
            'port': 'Порт',
            'type': 'Тип',
        }

        table = response.xpath('//table[@class="proxy__t"]')
        htp = HorizontalTableParser(table_selector=table, data_map=data_map)
        for item in htp.get_item_list():
            yield item

        next_page_xpath = '//div[@class="proxy__pagination"]//' \
                          'li[@class="arrow__right"]/a/@href'
        next_page_url = response.xpath(next_page_xpath).extract_first()
        if next_page_url:
            url = response.urljoin(next_page_url)
            yield Request(url=url, callback=self.parse)


class ProxyFreshProxySpider(Spider):  # TODO: Парсить по категориям
    """Spider для парсинга сайта proxy-fresh.ru
    Примечания:
    - Бесплатно просмотреть можно только первых 50 страниц
    - На странице 20 прокси (20 * 50 = 1000 адресов)
    """

    name = 'proxy_fresh_proxy'
    start_urls = ['http://proxy-fresh.ru/?page=1']

    def parse(self, response):
        table = response.xpath('//table')
        data_map = {
            'port': 'Порт',
            'ip': 'IP',
            'protocol': 'Протокол',
            'ping': 'Отклик',
            'country': 'Страна',
        }
        htp = HorizontalTableParser(table_selector=table, data_map=data_map)
        for item in htp.get_item_list():
            yield item

        next_page_xpath = './/div[contains(@class, "active item")]/' \
                          'following-sibling::*[1]/@href'
        next_page_url = table.xpath(next_page_xpath).extract_first()
        if next_page_url:
            url = response.urljoin(next_page_url)
            yield Request(url=url, callback=self.parse)


class FreeproxySpider(Spider):
    """Spider для парсинга сайта freeproxy-list.ru
    Примечания:
    - Порты и ip-адреса полностью показаны только на первой странице
    - Порты выводятся с помощью js (document.write)
    """

    name = 'freeproxy_list_proxy'
    start_urls = ['http://www.freeproxy-list.ru/']

    def parse(self, response):
        # Перебираем весь js код в поисках переменных
        js_var_a = None
        js_var_f = None
        for js_code in response.xpath('//script/text()').extract():
            if not js_var_a:
                js_var_a = re.findall(r'var a = "(\d+)"', js_code)
            if not js_var_f:
                js_var_f = re.findall(r'var f\d+ = (\d+)', js_code)
        # TODO: Рефакторить!

        def get_port(text):  # TODO: Рефакторить!
            magic_number = re.findall(r'(\d+)-a', text)
            if magic_number and js_var_a and js_var_f:
                port = int(magic_number[0])-int(js_var_a[0])-int(js_var_f[0])
                return str(port)
            return ''

        table = response.xpath('//div[contains(@class, "container")]')
        data_map = {
            'availability': 'Доступ\xadность (%)',
            'last_test': 'Время последнего тестирования',
            'country': 'Страна',
            'anonymouse': 'Аноним\xadный',
            'ip': 'IP адрес',
            'ping': 'Время ответа (мсек)',
            'port': 'Порт',
        }

        htp = HorizontalTableParser(table_selector=table, data_map=data_map)
        htp.line_xpath = './/*[contains(@class, "row")]'
        htp.row_xpath = './/*[contains(@class, "th") or contains(@class, "td")]'
        for item in htp.get_item_list():
            item['port'] = get_port(item['port'])
            yield item


class ProxySaleSpider(Spider):
    """Spider для парсинга сайта free.proxy-sale.com
    Примечания:
    - Порт указывается на картинке
    - Есть форма для фильтрации по странам, анонимности и протоколу
    - Сайт работает очень медленно
    """

    name = 'proxy_sale_proxy'
    start_urls = ['http://free.proxy-sale.com/?port%5B0%5D=http']

    def parse(self, response):
        table = response.xpath('//*[@class="container"]//table')
        data_map = {
            'ip': 'IP адрес',
            'x': '',
            'anonymouse': 'Анонимность',
            'type': 'Тип',
            'ping': 'Проверено',
            'speed': 'Скорость',
            'country': 'Страна',
            'port': 'Порт',  # TODO: Необходимо распознавать порт с картинки
        }

        htp = HorizontalTableParser(table_selector=table, data_map=data_map)
        for item in htp.get_item_list():
            yield item

        next_page_xpath = '//*[@class="pagination"]' \
                          '//*[@class="pag-bg active"]/parent::*' \
                          '/following-sibling::*[1]//a/@href'
        next_page_url = response.xpath(next_page_xpath).extract_first()
        if next_page_url:
            url = response.urljoin(next_page_url)
            yield Request(url=url, callback=self.parse)


class FreeProxyListSpider(Spider):
    """Spider для парсинга сайта freeproxylists.net
    Примечания:
    - Есть капча от google. Появляется если часто парсить сайт.
    - Есть форма для фильтрации по странам, порту, протоколу и анонимности.
    """

    name = 'free_proxy_list_proxy'
    start_urls = [
        'http://www.freeproxylists.net/ru/?c=&pt=&pr=HTTPS'
        '&a%5B%5D=0&a%5B%5D=1&a%5B%5D=2&u=0'
    ]

    def parse(self, response):
        table = response.xpath('//table[@class="DataGrid"]')
        data_map = {
            'protocol': 'Протокол',
            'port': 'Порт',
            'nalichie': 'Наличие',
            'ip': 'IP адрес',
            'city': 'Город',
            'country': 'Страна',
            'region': 'Регион',
            'anonymous': 'Анонимность',
        }

        htp = HorizontalTableParser(table_selector=table, data_map=data_map)
        for item in htp.get_item_list():
            unquoted_ip = parse.unquote(item['ip'])
            ip_list = re.findall(r'>(.*)<', unquoted_ip)
            if ip_list:
                item['ip'] = ip_list[0]
            # TODO: Убрать лишние item'ы (рекламные блоки)
            # if 'ip' in item.keys() and 'port' in item.keys():
            yield item

        next_page_xpath = '//*[@class="page"][1]/*[@class="current"]' \
                          '/following-sibling::*[1]/@href'
        next_page_url = response.xpath(next_page_xpath).extract_first()
        if next_page_url:
            url = response.urljoin(next_page_url)
            yield Request(url=url, callback=self.parse)

# TODO: Впилить парсер для 'https://best-proxies.ru/proxylist/free/'
