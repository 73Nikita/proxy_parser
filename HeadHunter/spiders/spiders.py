from scrapy import Spider, Request
from ..items import VacancySnapshotItem, VacancySnapshotItemLoader
import json


def response_to_json(response, encoding='utf-8'):
    return json.loads(response.body.decode(encoding))


class HeadHunterSpider(Spider):
    name = 'head_hunter'

    custom_settings = {
        'ITEM_PIPELINES': {
            'HeadHunter.pipelines.HeadhunterPipeline': 300,
        }
    }

    vacancies_url = 'https://api.hh.ru/vacancies/?area={area}&page={page}&per_page=200&industry={industry}'
    vacancy_url = 'https://api.hh.ru/vacancies/{id}'
    industries_url = 'https://api.hh.ru/industries/'

    industries = []
    areas = ['98']

    items_parsed = 0

    def start_requests(self):
        # url = self.vacancies_url.format(page=1)
        # yield Request(url=url, callback=self.parse)

        url = 'https://api.hh.ru/vacancies/22226922'
        yield Request(url=url, callback=self.parse_vacancy)

        # url = self.industries_url
        # yield Request(url=url, callback=self.parse_industries)

    def parse_industries(self, response):
        data = response_to_json(response)
        for industry in data:
            self.industries.append(industry['id'])

        for area in self.areas:
            for industry in self.industries:
                url = self.vacancies_url.format(area=area, page=0,
                                                industry=industry)
                yield Request(url=url, callback=self.parse_vacancy_list,
                              meta={'area': area, 'industry': industry})

    def parse_vacancy_list(self, response):
        data = response_to_json(response)

        for item in data['items']:
            vacancy_url = self.vacancy_url.format(id=item['id'])
            yield Request(url=vacancy_url, callback=self.parse_vacancy)

        area = response.meta['area']
        industry = response.meta['industry']

        page_count = data['pages']
        page = data['page']
        if page < page_count:
            url = self.vacancies_url.format(area=area, page=page+1,
                                            industry=industry)
            yield Request(url=url, callback=self.parse_vacancy_list,
                          meta=response.meta)

    def parse_vacancy(self, response):
        data = response_to_json(response)

        item_loader = VacancySnapshotItemLoader()
        item_loader.add_value("requirement", data['description'])
        item_loader.add_value("responsibility", data['description'])
        item_loader.add_value("conditions", data['description'])
        item = item_loader.load_item()

        item['alternate_url'] = data['alternate_url']
        item['position'] = data['name']

        salary = data.get('salary') or {}
        item['salary_from'] = salary.get('from')
        item['salary_to'] = salary.get('to')

        item['city'] = data['area']['name']
        item['experience'] = data['experience']['id']

        employer = data.get('employer') or {}
        item['company_alternate_url'] = employer.get('alternate_url', '')
        item['company_name'] = employer.get('name', '')

        employment = data.get('employment') or {}
        item['employment'] = employment.get('id')

        return item
