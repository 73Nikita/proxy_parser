from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose
from scrapy.http import HtmlResponse


def get_description_serializer(description_element):
    """Фабрика создающая процессоры для ItemLoader'а"""
    finding_xpath = '//strong[contains(., "{element}")]/following-sibling::ul'

    def serializer(value):
        response = HtmlResponse(url='url', body=value, encoding='utf-8')
        xpath = finding_xpath.format(element=description_element)
        data = response.xpath(xpath).extract_first()
        return data or ''

    return serializer


class VacancySnapshotItem(Item):
    """Item снимка вакансии"""
    company_alternate_url = Field()
    company_name = Field()
    alternate_url = Field()
    position = Field()
    salary_from = Field()
    salary_to = Field()
    city = Field()
    experience = Field()
    requirement = Field()  # Требования
    responsibility = Field()  # Обязанности
    conditions = Field()  # Условия
    employment = Field()  # Тип занятости


class VacancySnapshotItemLoader(ItemLoader):
    """ItemLoader для загрузки и обработки полей Item'а снимка вакансии"""
    default_item_class = VacancySnapshotItem
    requirement_in = MapCompose(get_description_serializer('Требования'))
    responsibility_in = MapCompose(get_description_serializer('Обязанности'))
    conditions_in = MapCompose(get_description_serializer('Условия'))


class Proxy(Item):
    """Items для парсеров собирающих адреса прокси серверов"""
    pass


class MagicItem(Item):
    """Класс Item'а с неограниченным количеством полей"""
    def __setitem__(self, key, value):
        if key not in self.fields:
            self.fields[key] = Field()
        super(MagicItem, self).__setitem__(key, value)
