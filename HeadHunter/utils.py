"""
Горизонтальная таблица выглядит следующим образом:
----------------------------------------------------
| Key 1          | Key 2          | Key 3          |
|----------------|----------------|----------------|
| Line 1 Value 1 | Line 1 Value 1 | Line 1 Value 1 |
| Line 2 Value 1 | Line 1 Value 1 | Line 1 Value 1 |
----------------------------------------------------

Вертикальная таблица выглядит следующим образом:
----------------------------------------------------
| Key 1      | Column 1 Value 1 | Column 2 Value 1 |
| Key 2      | Column 1 Value 2 | Column 2 Value 2 |
| Key 3      | Column 1 Value 3 | Column 2 Value 3 |
----------------------------------------------------

Частным случаем вертикальных таблиц являются таблицы состоящие из двух столбцов
"""
import logging
from w3lib import html

from collections import namedtuple
from .items import MagicItem


class ClearDataMixin:
    """
    Класс примесь, который позволяет очистить текст от лишних пробелов,
    символов табуляции.
    """

    @staticmethod
    def clear_data(dirt_text):
        """
        Очистка содержимого html тэга от лишних пробелов, вложенных тэгов,
        символов табуляции
        :param dirt_text: (строка) текст, котрый необходимо очистить
        :return: (строка) очищенный текст
        """
        # Метод не должен падать при пустом входном аргументе
        if not dirt_text:
            return ''
        # Удаляем комментарии
        text = html.remove_comments(dirt_text)
        # Меняем тэги на пробелы
        text = html.replace_tags(text, ' ')
        # Меняем символы табуляции на пробелы
        text = html.replace_escape_chars(text, replace_by=' ')
        # Устраняем все лишние пробелы между словами
        text = ' '.join(text.split())

        return text


class TableParser(ClearDataMixin):
    """Абстрактный класс парсера таблиц.
    Содержит общие методы для обработки селекторов таблиц.
    """
    item_class = MagicItem  # Класс Item'а по умолчанию

    line_xpath = './/tr'  # Xpath для обозначения строк в селекторе
    row_xpath = './/td|th'  # Xpath для обозначения ячеек в таблице

    _table_array = None  # Переменная для хранения обработанной таблицы

    equality = False  # Точное соответствие строк

    def __init__(self, table_selector, data_map, item_class=None, **kwargs):
        """
        :param table_selector: Selector
        :param data_map: карта данных {'key': 'title'}, где
        key - название поля в item'e
        title - название поля в таблице на сайте
        :param item_class: класс item'а используемый для заполнения данными.
        Если класс не указан используется класс Item'а по умолчанию
        :param kwargs: дополнительные аргументы
        """
        self.table_selector = table_selector
        self.data_map = data_map

        if item_class:
            self.item_class = item_class

        if 'line_xpath' in kwargs:
            self.line_xpath = kwargs['line_xpath']
        if 'row_xpath' in kwargs:
            self.row_xpath = kwargs['row_xpath']
        if 'equality' in kwargs:
            self.equality = kwargs['equality']

    def __setattr__(self, key, value):
        if key == 'table_selector':  # Если селектор меняется у убъекта класса
            self._table_array = None  # Очищаем "кешированную" таблицу
        super(TableParser, self).__setattr__(key, value)

    def get_table_from_selector(self):
        """Метод для получения таблицы в виде двумерного массива
        :return: список из списков содержащих строки текст из ячеек таблицы
        """
        if self._table_array:  # Если таблица уже была обработана
            return self._table_array  # Просто возвращаем кешированную таблицу
        lines = []
        for tr in self.table_selector.xpath(self.line_xpath):
            rows = []
            for td in tr.xpath(self.row_xpath).extract():
                rows.append(self.clear_data(td))
            if rows:
                lines.append(rows)
        self._table_array = lines  # Кешируем таблицу
        return lines

    def get_field_indexes(self):
        """Создание новой карты данных

        :return: словарь {key, index}, где
        key - название поля в классе Item
        index - номер столбца с данными в таблице начиная с 0
        """
        new_data_map = {}
        for item_field_name, table_column_names in self.data_map.items():
            for table_column_name_2, index in self.get_head().items():
                if not type(table_column_names) == list:
                    table_column_names = [table_column_names]
                for table_column_name in table_column_names:
                    a = table_column_name.lower()
                    b = table_column_name_2.lower()
                    e = self.equality
                    if (e and a == b) or (not e and b.find(a) != -1):
                        new_data_map[item_field_name] = index
                        break
        return new_data_map

    def get_head(self):
        """Метод для получения заголовков таблицы"""
        raise NotImplementedError("Не реализован метод get_head")

    def get_body(self):
        """Метод для получения тела таблицы"""
        raise NotImplementedError("Не реализован метод get_body")

    def _fill_item(self, data_list: list):
        """Заполняет item данными
        :param data_list: список с данными.
        :return: объект item заполненный данными из списка data_list
        """
        field_indexes = self.get_field_indexes()
        data = {}
        for item_field_name, index in field_indexes.items():
            if index < len(data_list):
                data[item_field_name] = data_list[index]
        if data:
            item = self.item_class(**data)
            return item
        return None

    def get_named_tuples(self):
        """ Получение списка именованых кортежей, где каждый кортеж - строка
        таблицы

        Названия элементов таблицы берется из карты данных, которая передается
        в конструктор класса. Пример использования:
        fields_map = {qty': 'Количество',}
        table_parser = LotItemTableParser(table_sel, fields_map)
        rows_list = table_parser.get_named_tuples()
        for row in rows_list:
            if hasattr(row, 'qty'):
                lotitem_item['qty'] = row.qty
        :return: Список именованых кортежей
        """
        tuple_list = []
        field_indexes = self.get_field_indexes()
        Row = namedtuple('TableRow', list(field_indexes.keys()))
        for line in self.get_body():
            data = {}
            for item_field_name, index in field_indexes.items():
                data[item_field_name] = line[index]
            if data:
                row = Row(**data)
                tuple_list.append(row)

        return tuple_list

    def get_item_list(self):
        """Возвращает список заполненных item'ов
        :return: список объектов Item
        """
        item_list = []
        for line in self.get_body():
            item = self._fill_item(line)
            if item:
                item_list.append(item)
        return item_list

    def get_item(self):
        """Метод возвращает только первый item.
        Подходит для вертикальных таблиц состоящих из двух столбцов
        """
        item_list = self.get_item_list()
        if item_list:
            return item_list[0]
        return {}


class HorizontalTableParser(TableParser):
    """Парсер горизонтальных таблиц
    Способ использования парсера:
    htp = HorizontalTableParser(  # Создание объекта парсера
        table_selector=table_selector,
        data_map=data_map,
        item_class=item_class
    )
    htp.get_item_list()  # Получение списка item'ов заполненных данными таблицы
    """

    def get_head(self):
        """Реализация метода для получения шапки таблицы"""
        table_array = self.get_table_from_selector()
        head_dict = {}
        if table_array:
            for i, td in enumerate(table_array[0]):
                head_dict[self.clear_data(td)] = i
        return head_dict

    def get_body(self):
        """Реализация метода для получения тела таблицы"""
        data = self.get_table_from_selector()[1:]
        return data


class VerticalTableParser(TableParser):
    """Парсер вертикальных таблиц
    Способ использования парсера:
    vtp = VerticalTableParser(  # Создание объекта парсера
        table_selector=table_selector,
        data_map=data_map,
        item_class=item_class
    )
    vtp.get_item_list()  # Получение списка item'ов заполненных данными таблицы
    """

    def get_head(self):
        """Реализация метода для получения шапки таблицы"""
        table_array = self.get_table_from_selector()
        head_dict = {}
        for i, line in enumerate(table_array):
            if line:
                key = self.clear_data(line[0])
                key = '' if key is None else key
                head_dict[key] = i
        return head_dict

    def get_body(self):
        """Реализация метода для получения тела таблицы"""
        data = []
        table = self.get_table_from_selector()
        if table:
            count = len(table[0])
            for index in range(count):
                if index == 0:
                    continue
                new_line = []
                for line in table:
                    try:
                        new_line.append(line[index])
                    except IndexError as e:
                        logging.warning("Error occurred by parsing "
                                        "line. Error message: {}".format(e))
                data.append(new_line)
        return data
